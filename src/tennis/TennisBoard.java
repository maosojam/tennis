package tennis;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class TennisBoard extends JFrame implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TennisPanel contentPane;
	
	public TennisBoard(ArrayList<ComponenteCancha> componentes) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 450, 338);
		setSize(600, 700);
		contentPane = new TennisPanel(componentes);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	}
	public void setComponentes(ArrayList<ComponenteCancha> componentes){
		contentPane.setComponentes(componentes);
	}
	public void empezarPartido(){
		Personaje personaje = new Personaje(50, 50);
	}
	public JPanel getContentPanel(){
		return contentPane;
	}
}
