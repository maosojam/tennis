package tennis;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Personaje extends ComponenteCancha implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private BufferedImage sprite;
	private Movimiento ladoCancha;
	//private Animacion animacion;
	//private BufferedImage[] movimientoSprites;
	//private BufferedImage[] golpeSprites;
	
	public Personaje(int posX, int posY){
		//CampoDeJuego campoDeJuego = CampoDeJuego.getInstance();
		//campoDeJuego.getCancha()
		//this.posX = posX;
		//this.posY = posY;
		radio = 7;
		setPosicion(posX, posY);
		this.ladoCancha = posY < 250 ? Movimiento.NORTE : Movimiento.SUR;
		try {
			Image img =  ImageIO.read(new File("archivos/personaje.png")).getScaledInstance(20, 30, Image.SCALE_SMOOTH);
			sprite = new ImageIcon(img);
			//System.out.println(sprite);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//movimientoSprites = new BufferedImage[];
	}
	public void golpeConRaqueta(){
		Pelota pelota = Pelota.getInstance();
		Punto2D puntoPersonaje = new Punto2D(posX,posY);
		Punto2D puntoPelota = new Punto2D(pelota.getPosX(),pelota.getPosY());
		Double velocidad;
		Movimiento sentido = ladoCancha == Movimiento.SUR ? Movimiento.NORTE : Movimiento.SUR;
		double distancia = puntoPersonaje.distancia(puntoPelota);
		if(distancia < 10){
			velocidad = Math.random()*10 ;
			//System.out.println(velocidad);
			velocidad = velocidad < 10 ? 10 : velocidad;
			pelota.movimiento(sentido,velocidad.intValue());
			//golpeDebil
		}else if(distancia <20){
			velocidad = Math.random()*100;
			velocidad = velocidad < 25 ? 25: velocidad;
			pelota.movimiento(sentido,velocidad.intValue());
			//golpefuerte
		}else if(distancia < 30){
			velocidad = Math.random()*50;
			velocidad = velocidad < 15 ? 15: velocidad;
			pelota.movimiento(sentido,velocidad.intValue());
			//golpeMedio
		}
	}
	
	public void movimiento(Movimiento direccion){
		switch (direccion) {
		case SUR:
			moverComponente(0, 5);
			break;
		case NORTE:
			moverComponente(Math.PI, 5);
			break;		
		case ESTE:
			moverComponente(Math.PI/2, 5);
			break;		
		case OESTE:
			moverComponente(3.0/2*Math.PI, 5);
			break;
		case SUDESTE:
			moverComponente(Math.PI/4, 5);
			break;
		case SUDOESTE:
			moverComponente(7.0/4*Math.PI, 5);
			break;		
		case NORESTE:
			moverComponente(3.0/4*Math.PI, 5);
			break;		
		case NOROESTE:
			moverComponente(5.0/4*Math.PI, 5);
			break;
		default:
			break;
		}
	}
	public ImageIcon getSprite(){
		return sprite;
	}
	public void saque(){
		golpeConRaqueta();
	}
	@Override
	public void colision(ComponenteCancha componente) {
		// TODO Auto-generated method stub
		
	}
}
