package tennis;
import java.awt.image.*;
public class Animacion {
	private BufferedImage[] frames;
	private int currentFrame;
	
	private long startTime;
	private long delay;
	
	public Animacion(){
		
	}
	
	public void setFrame(BufferedImage[] images){
		frames = images;
		if(currentFrame >= frames.length){
			currentFrame = 0;
		}
	}
	
	public void setDelay(long d){
		delay = d;
	}
	
	public void update(){
		if(delay == -1){
			return;
		}
		long tiempoTranscurrido = (System.nanoTime() - startTime) / 10000000;
		if(tiempoTranscurrido > delay){
			currentFrame++;
			startTime = System.nanoTime();
		}
		if(currentFrame == frames.length){
			currentFrame = 0;
		}
	}
	
	public BufferedImage getImage(){
		return frames[currentFrame];
	}
}
