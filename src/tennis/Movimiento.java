package tennis;

public enum Movimiento {
	NORTE,
    SUR,
    ESTE,
    OESTE,
    SUDOESTE,
    SUDESTE,
    NORESTE,
    NOROESTE;
}
