package tennis;

public class Circulo {
	private Punto2D centro;
	private double radio;
	
	public Circulo(Punto2D centro, double radio) {
		this.centro = centro;
		this.radio = radio;
	}
	public Punto2D getCentro() {
		return centro;
	}
	public void setCentro(Punto2D centro) {
		this.centro = centro;
	}
	public double getRadio() {
		return radio;
	}
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
	public boolean contiene(Punto2D punto){
		double distanciaEntrePuntos = this.centro.distancia(punto);
		return distanciaEntrePuntos <= this.radio;
	}
	
	public boolean intersectaCon(Circulo otroCirculo){
		//System.out.println(this.radio+ ", " +otroCirculo.radio);
		//System.out.println(this.centro.distancia(otroCirculo.centro));
		double sumaDeRadios = this.radio + otroCirculo.radio;
		double distanciaEntreCentros = this.centro.distancia(otroCirculo.centro);
		return distanciaEntreCentros <= sumaDeRadios;
	}
}
