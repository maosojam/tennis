package tennis;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Observer;

import javax.swing.ImageIcon;

public abstract class ComponenteCancha implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int posX;
	protected int posY;
	protected int radio;
	protected ImageIcon sprite;
	//protected int spriteHeight;
	//protected int spriteWidth;
	public abstract void colision(ComponenteCancha componenteCancha);
	public int getPosX() {
		return posX;
	}
	public int getPosY() {
		return posY;
	}
	public int getSpriteHeight() {
		return sprite.getIconHeight();
	}
	public int getSpriteWidth() {
		return sprite.getIconWidth();
	}
	public int getRadio() {
		return radio;
	}
	public void setPosX(int posX) {
		this.posX = posX;
		// TODO Auto-generated method stub
		
	}
	public void setPosY(int posY) {
		this.posY = posY;
		// TODO Auto-generated method stub
		
	}
	public ImageIcon getSprite() {
		return sprite;
	}
	public boolean moverComponente(double angulo,double velocidad){
	    double dx = velocidad * Math.sin(angulo);
	    double dy = velocidad * Math.cos(angulo);
		return CampoDeJuego.setPosicionComponente(this, (int)(posX+dx), (int)(posY+dy));
	}
	public boolean setPosicion(int posX, int posY){
		//CampoDeJuego campoDeJuego = CampoDeJuego.getInstance();
		//ComponenteCancha[][] cancha = campoDeJuego.getCancha();
		//CampoDeJuego.setPosicionComponente(null, this.getPosX(), this.getPosY());
		
		//cancha[this.getPosX()][this.getPosY()] = null;
		//if( posX < cancha.length && posX >= 0 && posY < cancha[posX].length && posY >= 0 && cancha[posX][posY] == null){
		return CampoDeJuego.setPosicionComponente(this,posX,posY);

	}
	
	
}
