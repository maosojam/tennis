package tennis;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.Timer;

public class Pelota extends ComponenteCancha implements Serializable{ /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//falta hacerlo
	private Timer timer;
	public static Pelota pelota = new Pelota();
	
	private Pelota(){
		radio = 5;
		try {
			Image img =  ImageIO.read(new File("archivos/pelota.png")).getScaledInstance(10, 10, Image.SCALE_SMOOTH);
			sprite = new ImageIcon(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void movimiento(Movimiento sentido, int velocidad){ // sentido (norte,sur)
		int tiempoEspera = 100/velocidad;
		Random r = new Random();
		double result;
		double ang_menor = Math.atan(posX/ ( (576.0/2) - posY ) );
		double ang_mayor = Math.atan(( (432.0) - posX ) / (  (576.0/2) - posY ) );
		if(sentido == Movimiento.NORTE){
			ang_menor = 180 - (ang_menor * 180.0 / Math.PI);
			ang_mayor = 180 + (ang_mayor * 180.0 / Math.PI);
		}else{
			ang_menor = 360 - (ang_menor * 180.0 / Math.PI);
			ang_mayor = 360 + (ang_mayor * 180.0 / Math.PI);
		}
		result = r.nextDouble() * (ang_mayor - ang_menor) + ang_menor;
		
		timer = new Timer(tiempoEspera, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		    	if(!moverComponente(Math.toRadians(result), 3)){
		    		System.out.println("termine");
		    		timer.stop();
		    	}
		    }    
		});	
		timer.start();
		
	}
	
	public static Pelota getInstance(){
		return pelota;
	}
	@Override
	public void colision(ComponenteCancha componente) {
		// TODO Auto-generated method stub
		
	}
}
