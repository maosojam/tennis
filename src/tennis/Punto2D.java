package tennis;

public class Punto2D {
	private double x;
	private double y;
	
	public Punto2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Punto2D(){
		this.x = 0;
		this.y = 0;
	}
	

	public double modulo(){
		return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y, 2));
	}
	
	public double distancia(Object punto){
		Punto2D other = (Punto2D) punto;
		return Math.sqrt(Math.pow(this.x - other.x,2) + Math.pow(this.y - other.y, 2));
	}

	@Override
	public String toString() {
		return "Punto2D [ x= " + x + ", y=" + y + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto2D other = (Punto2D) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}
	
	public Punto2D clone(){
		return new Punto2D(this.x,this.y);
	}
	
	public void desplazamiento(Object other){
		Punto2D punto = (Punto2D) other;
		this.x += punto.x ;
		this.y += punto.y;
	}
}
