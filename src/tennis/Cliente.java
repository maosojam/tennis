package tennis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.RepaintManager;
import javax.swing.Timer;

import com.google.gson.Gson;

public class Cliente extends Thread implements KeyListener{
	private Personaje personaje;
	private int idPersonaje;
	private ArrayList<Short> keyPressed;
	private ArrayList<ComponenteCancha> componentes;
	private Socket serverSocket;
	private Gson json;
	public Cliente(){
		keyPressed = new ArrayList<>();
		json = new Gson();
		//System.out.println("asd");
		//new TennisBoard(componentes);
	}
	public void run(){
		try {
			serverSocket = new Socket("192.168.100.111", 8113);
			/*DataOutputStream dataOutput = new DataOutputStream(serverSocket.getOutputStream());
			dataOutput.writeUTF(getName());
			DataInputStream f = new DataInputStream(serverSocket.getInputStream());
			idPersonaje = Integer.parseInt(f.readUTF());
			f.close();*/
			/*synchronized (this) {
				yaFuncionando = true;
				notifyAll();
			}*/
			
			//File file = new File("myfile.dat");
			ObjectInputStream in;
			in = new ObjectInputStream(serverSocket.getInputStream());
			idPersonaje = Integer.parseInt(in.readUTF());
			//String mensaje = f.readUTF();
			try {
				componentes = (ArrayList<ComponenteCancha>) in.readObject();
			} catch (ClassNotFoundException | EOFException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			};
			TennisBoard tennisBoard = new TennisBoard(componentes);
			TennisPanel tennisPanel;
			tennisBoard.setVisible(true);
			tennisBoard.setFocusable(true);
			tennisBoard.requestFocus();
			tennisBoard.addKeyListener(this);
			
			Timer timer = new Timer(20, new ActionListener() {
			    public void actionPerformed(ActionEvent evt) {
			    	moverPersonaje();
			    	//tennisBoard.repaint();
			    }    
			});	
			timer.start();
			
			
			
			while(true){
				//mensaje = f.readUTF();
				//tennisBoard = json.fromJson(mensaje, TennisBoard.class);
				//tennisBoard.repaint();
				
				
				
				try {
					//tennisPanel = (TennisPanel) in.readObject();
					componentes = (ArrayList<ComponenteCancha>) in.readObject();
					tennisBoard.setComponentes(componentes);
					//System.out.println("me da el panel");
				    /*tennisBoard.setContentPane(tennisPanel);
				    if(!tennisBoard.isVisible()){
					    tennisBoard.setVisible(true);
					    tennisBoard.setFocusable(true);
					    tennisBoard.requestFocus();
					    tennisBoard.addKeyListener(this);
				    }*/
					tennisBoard.repaint();
				    //tennisBoard.repaint();
				   
				} catch (EOFException e2) {
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			}
			//in.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*ArrayList<ComponenteCancha> componentes = new ArrayList<>();
		personaje = new Personaje(50, 50);
		componentes.add(personaje);
		componentes.add(Pelota.getInstance());
		Pelota.getInstance().setPosicion(100, 100);
		TennisBoard tennisBoard = new TennisBoard(componentes);
		tennisBoard.setVisible(true);
		tennisBoard.setFocusable(true);
		tennisBoard.requestFocus();
		tennisBoard.addKeyListener(this);
		Timer timer = new Timer(20, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		    	moverPersonaje();
		    	tennisBoard.repaint();
		    }    
		});	
		timer.start();*/
	}
	
	private void moverPersonaje(){
		if(!keyPressed.isEmpty()){
			String[] mensaje = new String[3];
			mensaje[0] = Integer.toString(idPersonaje);
			switch (keyPressed.toString()) {
			case "[37]":
				//personaje.movimiento(Movimiento.OESTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.OESTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[38]":
				//personaje.movimiento(Movimiento.NORTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.NORTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[39]":
				//personaje.movimiento(Movimiento.ESTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.ESTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[40]":
				//personaje.movimiento(Movimiento.SUR);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.SUR";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[37, 38]":
				//personaje.movimiento(Movimiento.NOROESTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.NOROESTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[38, 39]":
				//personaje.movimiento(Movimiento.NORESTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.NORESTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[39, 40]":
				//personaje.movimiento(Movimiento.SUDESTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.SUDESTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			case "[37, 40]":
				//personaje.movimiento(Movimiento.SUDOESTE);
				mensaje[1] = "movimiento";
				mensaje[2] = "Movimiento.SUDOESTE";
				enviarMensaje(json.toJson(mensaje));
				break;
			
			default:
				break;
			}	
		}
		
	}
	public void enviarMensaje(String mensaje){
		/*synchronized(this){
			try {
				if(!this.yaFuncionando){
					wait();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		DataOutputStream dataOutput;
		try {
			dataOutput = new DataOutputStream(serverSocket.getOutputStream());
			//System.out.println(mensaje);
			dataOutput.writeUTF(mensaje);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Cliente().start();
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		//System.out.println(e);
		if( e.getKeyCode() == 68){
			//System.out.println("golpe");
			String[] mensaje = {Integer.toString(idPersonaje),"golpeConRaqueta"};
			enviarMensaje(json.toJson(mensaje));
			//personaje.golpeConRaqueta();
		}else if (keyPressed.indexOf((short) e.getKeyCode()) == -1){
			keyPressed.add((short) e.getKeyCode());
			keyPressed.sort(null);
		}
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("asd");
		//System.out.println(e);
		int index = keyPressed.indexOf((short) e.getKeyCode());
		//System.out.println(keyPressed);
		if(index != -1){
			keyPressed.remove(index);
		}
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		//System.out.println(e);
	}

}
