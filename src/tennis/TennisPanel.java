package tennis;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class TennisPanel extends JPanel {
	ArrayList<ComponenteCancha> componentes;
	public static final int height = 700;
	public static final int width = 600;
	public TennisPanel(ArrayList<ComponenteCancha> componentes){
		this.componentes = componentes;
		setLayout(null);
        setSize(width, height);
	}
	public void setComponentes(ArrayList<ComponenteCancha> componentes){
		this.componentes = componentes;
	}
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        CampoDeJuego campoDeJuego = CampoDeJuego.getInstance();
        g.drawImage(campoDeJuego.getImage(), this.getWidth()/2-campoDeJuego.getImageWidth()/2, this.getHeight()/2-campoDeJuego.getImageHeight()/2,campoDeJuego.getImageWidth(), campoDeJuego.getImageHeight(), null); 
        for (ComponenteCancha componenteCancha : componentes) {
        	componenteCancha.getSprite().paintIcon(this, g, componenteCancha.getPosX() - (componenteCancha.getSpriteWidth()/2), componenteCancha.getPosY() - (componenteCancha.getSpriteHeight() / 2));
		}
	}
}
