package tennis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Tennis {
	public static void main(String[] args){
		Gson gson = new GsonBuilder().create();
		Personaje personaje1 = new Personaje(50, 400);
		gson.toJson(personaje1);
		Personaje personaje2 = new Personaje(200, 50);
		Pelota pelota = Pelota.getInstance();
		pelota.setPosicion(51, 400);
		ArrayList<ComponenteCancha> componentes = new ArrayList<>();
		componentes.add(personaje1);
		componentes.add(personaje2);
		componentes.add(pelota);
		
		TennisBoard tennisBoard = new TennisBoard(componentes);
		tennisBoard.setVisible(true);
		
		Timer timer = new Timer(10, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		    	tennisBoard.repaint();
		    }    
		});	
		timer.start();
		System.out.println("Aqwep");
		personaje1.golpeConRaqueta();
		//pelota.movimiento(Movimiento.SUDOESTE, 20);
		//JPanel panel = tennisBoard.getContentPanel();
		
	}
}
