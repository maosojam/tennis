package tennis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

import javax.swing.Timer;

import com.google.gson.Gson;

public class Servidor extends Thread{
	private ArrayList<ServerThread> serversThread;
	private static Servidor servidor = new Servidor();
	private ArrayList<ComponenteCancha> componentes;
	private Servidor(){
		componentes = new ArrayList<>();
		Pelota pelota = Pelota.getInstance();
		pelota.setPosicion(55, 55);
		componentes.add(pelota);
		this.start();
	}
	public void run(){
		ServerSocket server = null;
		serversThread = new ArrayList<ServerThread>();
		try {
			server = new ServerSocket(8113);
			Timer timer = new Timer(10, new ActionListener() {
			    public void actionPerformed(ActionEvent evt) {
			    	if(serversThread.size()>=2){
			    		for (ServerThread serverThread : serversThread) {
							serverThread.enviarComponentes(componentes);
						}
			    	}
			    }    
			});	
			timer.start();
			while(componentes.size() < 5){
				Socket cliente;
				try {
					cliente = server.accept();
					ServerThread serverThread = new ServerThread(cliente);
					serverThread.start();
					Personaje personaje = null;
					switch (componentes.size()) {
						case 1:
							personaje = new Personaje(50, 50);
							break;
						case 2:
							personaje = new Personaje(200, 50);
							break;
						case 3:
							personaje = new Personaje(50, 400);
							break;
						case 4:
							personaje = new Personaje(200, 400);
							break;
						default:
							break;
					}
					componentes.add(personaje);
					serverThread.enviarMensajeACliente(Integer.toString(componentes.indexOf(personaje)));
					serversThread.add(serverThread);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally {
		    try {  
		        server.close();
		    } catch(Exception e) {
		        // If you really want to know why you can't close the ServerSocket, like whether it's null or not
		    }
		}
		
	}
	public static Servidor getInstance(){
		return servidor;
	}
	public void cerrarServerThread(ServerThread st) {
		serversThread.remove(st);		
	}
	public static void main(String[] args){
		Servidor.getInstance();
	}
	public void procesarMensaje(String mensaje) {
		Gson json = new Gson();
		String[] arrayMensaje = json.fromJson(mensaje, String[].class);
		int idComponente = Integer.parseInt(arrayMensaje[0]);
		if(idComponente>0){
			Personaje personaje = (Personaje) componentes.get(idComponente);
			switch (arrayMensaje[1]) {
			case "movimiento":
				switch (arrayMensaje[2]) {
				case "Movimiento.SUR":
					personaje.movimiento(Movimiento.SUR);
					break;
				case "Movimiento.NORTE":
					personaje.movimiento(Movimiento.NORTE);
					break;		
				case "Movimiento.ESTE":
					personaje.movimiento(Movimiento.ESTE);
					break;		
				case "Movimiento.OESTE":
					personaje.movimiento(Movimiento.OESTE);
					break;
				case "Movimiento.NOROESTE":
					personaje.movimiento(Movimiento.NOROESTE);
					break;
				case "Movimiento.NORESTE":
					personaje.movimiento(Movimiento.NORESTE);
					break;		
				case "Movimiento.SUDOESTE":
					personaje.movimiento(Movimiento.SUDOESTE);
					break;		
				case "Movimiento.SUDESTE":
					personaje.movimiento(Movimiento.SUDESTE);
					break;
				default:
					break;
				}
				break;
			case "golpeConRaqueta":
				personaje.golpeConRaqueta();
				break;

			default:
				break;
			}
		}
	}
	
}
