package tennis;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

import javax.management.NotCompliantMBeanException;

import com.google.gson.Gson;

public class ServerThread extends Thread{
	private Socket cliente;
	private Servidor servidor;
	private Gson json;
	private ObjectOutputStream ostream;
	public ServerThread(Socket cliente) {
		this.cliente = cliente;
		json = new Gson();
		servidor = Servidor.getInstance();
		//FileOutputStream f = null;
		try {
			//f = new FileOutputStream("myfile.dat");
			ostream = new ObjectOutputStream(cliente.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void run(){
		String nombreCliente = "";
		try {
			DataInputStream dataInput = new DataInputStream(cliente.getInputStream());
			nombreCliente = dataInput.readUTF();
			String mensaje ;
			while(true){
				mensaje = dataInput.readUTF();
				//servidor.enviarMensajeATodosLosClientes(nombreCliente + " : "+ mensaje);
				servidor.procesarMensaje(mensaje);
			}
		} catch (SocketException e) {
			
			System.out.println("salio el cliente "+ nombreCliente);
			servidor.cerrarServerThread(this);
			//servidor.enviarMensajeATodosLosClientes("salio el cliente "+ nombreCliente);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void enviarMensajeACliente(String mensaje) {
		try {
			//DataOutputStream dataOutput = new DataOutputStream(cliente.getOutputStream());
			ostream.writeUTF(mensaje);
			//dataOutput.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void enviarComponentes(ArrayList<ComponenteCancha> componentes) {
		// TODO Auto-generated method stub
		try {
			/*DataOutputStream dataOutput = new DataOutputStream(cliente.getOutputStream());
			//System.out.println(json.toJson(componentes.get(0)));
			dataOutput.writeUTF(json.toJson(componentes));*/
			//tennisBoard.repaint();
			//ostream = new ObjectOutputStream(cliente.getOutputStream());
			Object object = componentes;  //where panel is a JPanel type object
			ostream.reset();
			ostream.writeObject(object);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void enviarBoard(TennisBoard tennisBoard) {
		// TODO Auto-generated method stub
		try {
			//DataOutputStream dataOutput = new DataOutputStream(cliente.getOutputStream());
			//System.out.println(json.toJson(componentes.get(0)));
			//System.out.println("me trabo");
			//dataOutput.writeUTF(json.toJson(tennisBoard));
			
			tennisBoard.repaint();
			Object object = tennisBoard.getContentPanel();  //where panel is a JPanel type object
			//ostream.reset();
			ostream.writeObject(object);
			//ostream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
