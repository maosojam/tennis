package tennis;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;


public class CampoDeJuego /*extends JPanel*/{
	//private static ComponenteCancha[][] cancha;
	private static ArrayList<ComponenteCancha> componentes;
	private static CampoDeJuego campoDeJuego = new CampoDeJuego();
	private BufferedImage image;
	private static int imageHeight = 576;
	private static int imageWidth = 432;
	public static int getImageHeight() {
		return imageHeight;
	}

	public BufferedImage getImage() {
		return image;
	}

	public static int getImageWidth() {
		return imageWidth;
	}

	/*public static boolean setPosicionComponente(ComponenteCancha componente, int posX, int posY){
		return true;
	}*/

	private CampoDeJuego() {
		//cancha = new ComponenteCancha[1000][1000];
		componentes = new ArrayList<>();
		try {                
          image = ImageIO.read(new File("archivos/campoDeJuego.png"));
         // setLayout(null);
         // setSize(432, 576);
       } catch (IOException ex) {
            // handle exception...
    	   ex.printStackTrace();
       }
	}
	/*@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0,430,545, null); // see javadoc for more info on the parameters            
    }*/
	
	public static CampoDeJuego getInstance(){
		return campoDeJuego;
	}
	
	/*public ComponenteCancha[][] getCancha(){
		return cancha;
	}*/
	public void anadirComponente(ComponenteCancha componenteCancha){
		componentes.add(componenteCancha);
	}

	public static boolean setPosicionComponente(ComponenteCancha componenteCancha, int posX, int posY) {
		/*if( posX < cancha.length && posX >= 0 && posY < cancha[posX].length && posY >= 0 && cancha[posX][posY] == null){
			cancha[componenteCancha.getPosX()][componenteCancha.getPosY()] = null;
			cancha[posX][posY] = componenteCancha;
			return true;
		}
		return false;*/
	
		if(!componentes.contains(componenteCancha)){
			componentes.add(componenteCancha);
		}
		System.out.println(posX);
		System.out.println(posY);
		int radio = componenteCancha.getRadio();
		if(posX-radio >= 0 && posX+radio < TennisPanel.width && posY-radio >= 0 && posY+radio < TennisPanel.height){
			//System.out.println(componentes);
			for (ComponenteCancha componente : componentes) {
				if(!componente.equals(componenteCancha)){
					System.out.println(componente);
					Circulo circuloC1 = new Circulo(new Punto2D(posX,posY),componenteCancha.getRadio());
					Circulo circuloC2 = new Circulo(new Punto2D(componente.getPosX(),componente.getPosY()),componente.getRadio());
					//System.out.println(circuloC1);
					//System.out.println(circuloC2);
					//if(componente.getPosX() == posX && componente.getPosY() == posY){
					if(circuloC1.intersectaCon(circuloC2)){
						System.out.println("colision");
						return false;
					}
				}
				
			}
			componenteCancha.setPosX(posX);
			componenteCancha.setPosY(posY);
			return true;
		}else{
			//System.out.println(posX);
			//System.out.println(posY);
			//System.out.println("me sali de la cancha");
			return false;
		}
		
	}
	

}